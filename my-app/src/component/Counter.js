import { useState } from "react";

export function Counter(props){

    const [value=0,setValue]=useState();
    const increase=()=> {
        setValue(value + 1);
        if(props&&props.onChange)
            props.onChange(1);
    }
    const decrease=()=> {
        setValue(value - 1);
        if(props&&props.onChange)
            props.onChange(-1);

    }
    return (
        <>
            <span>{value}</span>
            <button onClick={increase}>+</button>
            <button onClick={decrease}>-</button>
        </>
        
    )

  
}