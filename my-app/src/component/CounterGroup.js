import { Counter } from "../component/Counter";
import {CounterGroupSum} from "./CounterGroupSum";
export function CounterGroup(props){
    const build =()=>{
        return Array(props.size).fill(0).map((_,index)=><Counter key={index} onChange={props.onChange}/>);
    }

    return(
        <div>
            {build()}
        </div>
    )

}