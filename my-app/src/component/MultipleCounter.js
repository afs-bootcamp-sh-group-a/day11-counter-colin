import { useState } from "react";
import { CounterGroup } from "./CounterGroup";
import {CounterGroupSum} from "./CounterGroupSum";
export function MultipleCounter(){

    const [size=0,setSize]=useState();
    const [total=0,setTotal]=useState();
    const updateSize=(event)=>{
        setSize(parseInt(event.target.value));
    }
    const updateTotal=(newValue)=>{
        setTotal(total+parseInt(newValue));
    }
    return (
        <div>
            <input value={size} onChange={updateSize}></input>
            <CounterGroupSum value={total}></CounterGroupSum>
            <CounterGroup size={size} onChange={updateTotal}></CounterGroup>
        </div>
        
    )
}