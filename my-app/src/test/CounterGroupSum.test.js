import {MultipleCounter} from "../component/MultipleCounter";

const {render,screen, fireEvent}=require("@testing-library/react");
const { CounterGroupSum } = require("../component/CounterGroupSum");
describe("counter test",()=>{
    it("should show count value",()=>{
        render(<MultipleCounter></MultipleCounter>);
        var input=screen.getByRole("textbox")
        var size="2";
        fireEvent.change(input,{target:{value:size}});

        var plusButtons=screen.getAllByText("+");
        for(let i=0;i<plusButtons.length;i++)
            fireEvent.click(plusButtons[i]);
        var zeros=screen.getAllByText("2");
        expect(zeros.length).toBe(1);
    })
})