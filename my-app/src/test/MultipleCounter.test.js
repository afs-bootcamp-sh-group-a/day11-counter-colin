const {render,screen, fireEvent}=require("@testing-library/react");
const { MultipleCounter } = require("../component/MultipleCounter");
describe("counter test",()=>{
    it("should show zeros as init value",()=>{
        render(<MultipleCounter></MultipleCounter>);
        var input=screen.getByRole("textbox")
        var size="2";
        fireEvent.change(input,{target:{value:size}});
        var zeros=screen.getAllByText("+");
        expect(zeros.length).toBe(2);
    })
})