import { Counter } from "../component/Counter";
import {MultipleCounter} from "../component/MultipleCounter";
const {render,screen, fireEvent}=require("@testing-library/react")

describe("counter test",()=>{
    it("should show zero as init value",()=>{
        render(<Counter></Counter>);
        var zeros=screen.getAllByText("0");
        expect(zeros.length).toBe(1);
    })

    it("should increase when click plus button",()=>{

        render(<Counter></Counter>);


        var plusButton=screen.getByText("+");
        fireEvent.click(plusButton);
        var one=screen.getAllByText("1");
        expect(one.length).toBe(1);
    })
    it("should decrease when click minus button",()=>{

        render(<Counter></Counter>);


        var plusButton=screen.getByText("-");
        fireEvent.click(plusButton);
        var one=screen.getAllByText("-1");
        expect(one.length).toBe(1);
    })


}
)