import { CounterGroup } from "../component/CounterGroup";
const {render,screen, fireEvent}=require("@testing-library/react")

describe("counter test",()=>{
    it("should show zeros as init value",()=>{
        render(<CounterGroup size={3}/>);
        var zeros=screen.getAllByText("0");
        expect(zeros.length).toBe(3);
    })
})